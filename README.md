# Tools and Technologies used:
	Spring Boot 2.1.3.RELEASE
	PostgrePortable 2.2.1.0
	Java 8
	IntelliJ
	Tomcat 7.0.107
	Maven 3.6.3

# RequiredAPI
This API is used to get List of CryptoCurrency from coinmarketcap.com
by using generate token through register access it by login with a bearer token

# How to Use
Go to sandy directory
Run program: mvn spring-boot:run

# Screenshot testing with postman:

## Register
![picture](img/register.png)

## Login
![picture](img/login.png)

## GetListCryptoCurrency
![picture](img/getListCurrency.png)

