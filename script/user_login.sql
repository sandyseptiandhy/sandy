create table user_login
(
	id integer not null
		constraint user_login_pk
			primary key,
	user_name varchar not null,
	password varchar not null,
	token varchar not null,
	email varchar
);
/* sample data */
insert into public.user_login (id, user_name, password, token, email) values (1, 'sandy', 'password', 'daa598f6-62ce-4640-82de-51c2e42594fe', 'sandy@gmail.com');
