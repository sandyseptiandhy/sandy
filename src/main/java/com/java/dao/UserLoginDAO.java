package com.java.dao;

import com.java.model.UserLogin;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserLoginDAO extends CrudRepository<UserLogin, Long> {

    @Query(value = "SELECT u FROM UserLogin u where u.email = ?1 and u.password = ?2 ")
    Optional<UserLogin> login(String email, String password);

    Optional<UserLogin> findByToken(String token);

    @Query(value = "SELECT u FROM UserLogin u where u.userName = ?1 and u.password = ?2 ")
    Optional<UserLogin> register(String username, String email, String password);


}
