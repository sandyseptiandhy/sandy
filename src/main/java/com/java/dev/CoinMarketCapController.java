package com.java.dev;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.java.model.UserLogin;
import com.java.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class CoinMarketCapController {
    private static String uri = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest";
    private static String apiKey = "c79f47d5-44fd-4e10-9b22-42908901c44c";

    @Autowired
    private UserLoginService userLoginService;

    @GetMapping(value = "/api/cryptolist",produces = "application/json")
    public String getUserDetail() throws IOException, URISyntaxException {
        List<NameValuePair> paratmers = new ArrayList<NameValuePair>();
        return makeAPICall(uri,paratmers);
    }

    public static String makeAPICall(String uri, List<NameValuePair> parameters)
            throws URISyntaxException, IOException {
        String response_content = "";
        JsonObject result;
        URIBuilder query = new URIBuilder(uri);
        query.addParameters(parameters);

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(query.build());

        request.setHeader(HttpHeaders.ACCEPT, "application/json");
        request.addHeader("X-CMC_PRO_API_KEY", apiKey);

        CloseableHttpResponse response = client.execute(request);

        try {
            System.out.println(response.getStatusLine());
            HttpEntity entity = response.getEntity();
            response_content = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
            // parsing JSON
            result = new Gson().fromJson(response_content, JsonObject.class);
        } finally {
            response.close();
        }

        return result.toString();
    }

}
