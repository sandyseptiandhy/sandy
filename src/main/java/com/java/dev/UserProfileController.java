package com.java.dev;

import com.java.model.UserLogin;
import com.java.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserProfileController {

    @Autowired
    private UserLoginService userLoginService;

    @PostMapping("/register")
    public ResponseEntity<UserLogin> getToken(@RequestParam("username") final String username, @RequestParam("email") final String email, @RequestParam("password") final String password){
        UserLogin userLogin= userLoginService.register(username,email,password);
        if (userLogin == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(userLogin);
        }
    }

    @GetMapping(value = "/api/users/user/{id}",produces = "application/json")
    public UserLogin getUserDetail(@PathVariable Long id){
        return userLoginService.findById(id);
    }

    @PostMapping("/login")
    public ResponseEntity<UserLogin> login(@RequestParam("email") final String email, @RequestParam("password") final String password){
        UserLogin userLogin=  userLoginService.loginByEmailAndPass(email,password);
        if (userLogin == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(userLogin);
        }
    }
}