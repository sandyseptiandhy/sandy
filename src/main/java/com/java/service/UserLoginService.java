package com.java.service;

import com.java.model.UserLogin;
import org.springframework.security.core.userdetails.User;

import java.util.Optional;

public interface UserLoginService {

    String login(String email, String password);
    UserLogin loginByEmailAndPass(String email, String password);
    Optional<User> findByToken(String token);
    UserLogin findById(Long id);
    UserLogin register(String username, String email, String password);
}