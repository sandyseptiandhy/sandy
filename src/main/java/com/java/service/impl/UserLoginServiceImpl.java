package com.java.service.impl;

import com.java.dao.UserLoginDAO;
import com.java.model.UserLogin;
import com.java.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service("customerService")
public class UserLoginServiceImpl implements UserLoginService {

    @Autowired
    UserLoginDAO userLoginDAO;

    @Override
    public String login(String username, String password) {
        Optional<UserLogin> userLogin = userLoginDAO.login(username,password);
        if(userLogin.isPresent()){
            String token = UUID.randomUUID().toString();
            UserLogin custom= userLogin.get();
            custom.setToken(token);
            userLoginDAO.save(custom);
            return token;
        }

        return "";
    }

    @Override
    public UserLogin loginByEmailAndPass(String username, String password) {
        Optional<UserLogin> userLogin = userLoginDAO.login(username,password);
        if(userLogin.isPresent()){
            String token = UUID.randomUUID().toString();
            UserLogin custom= userLogin.get();
            custom.setToken(token);
            userLoginDAO.save(custom);
            return custom;
        }
        return null;
    }

    @Override
    public Optional<User> findByToken(String token) {
        Optional<UserLogin> userLogin= userLoginDAO.findByToken(token);
        if(userLogin.isPresent()){
            UserLogin customer1 = userLogin.get();
            User user= new User(customer1.getUserName(), customer1.getPassword(), true, true, true, true,
                    AuthorityUtils.createAuthorityList("USER"));
            return Optional.of(user);
        }
        return  Optional.empty();
    }

    @Override
    public UserLogin findById(Long id) {
        Optional<UserLogin> userLogin= userLoginDAO.findById(id);
        return userLogin.orElse(null);
    }

    @Override
    public UserLogin register(String username, String email, String password) {
        Optional<UserLogin> userLogin = userLoginDAO.login(email,password);
        if(!userLogin.isPresent()) {
            String token = UUID.randomUUID().toString();
            UserLogin custom = new UserLogin();
            custom.setUserName(username);
            custom.setToken(token);
            custom.setEmail(email);
            custom.setPassword(password);
            userLoginDAO.save(custom);
            return custom;
        }

        return null;
    }


}